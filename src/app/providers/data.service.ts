import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response } from '../models/request/user.interface';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  url = 'http://138.197.118.88/datacity-pruebas/public/users/service/get';

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get<Response>(this.url+'/subscribers');
  }

  getForm(){
    return this.http.get<Response>(this.url+'/formsPublicados');
  }
}
