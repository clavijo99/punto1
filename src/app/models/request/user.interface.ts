export interface Response{
    data: [{
       text: string;
       id: number;
    }];
    message: string;
    error: number;
}
