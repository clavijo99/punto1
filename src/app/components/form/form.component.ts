import { Component, OnInit } from '@angular/core';
import { DataService } from '../../providers/data.service';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  users: any[] = [];
  forms: any[] = [];
  _check: boolean = false;
  codeUser: number = 0;

  constructor(private dataService: DataService) {
    dataService.getUsers().subscribe(response => {
      this.users = response.data;
    });
    dataService.getForm().subscribe(response => {
      this.forms = response.data;
    })
  }


  ngOnInit(): void {
  }

  check(event: any): void {
    if (event.value == 'si') {
      this._check = true;
    } else {
      this._check = false;
    }
  }

  sendData(): void {}
}
